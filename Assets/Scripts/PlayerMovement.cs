﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D playerRigidBody;
    public float PlayerSpeed;
    
    private void FixedUpdate()
    {
        Vector2 movement = PlayerInput.current.MovementVector;
        if (movement != Vector2.zero)
        {
            playerRigidBody.AddForce(movement * PlayerSpeed);
        }
    }
}