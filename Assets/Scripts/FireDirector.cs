﻿using System;
using System.Collections;
using UnityEngine;

public class FireDirector : MonoBehaviour
{
    public GameObject[] Fires;
    public static FireDirector Current;

    private void Awake()
    {
        Current = this;
    }

    public void FireStateChanged()
    {
        StartCoroutine("RefreshFire");
    }

    public IEnumerator RefreshFire()
    {
        yield return new WaitForEndOfFrame();
        if (IsAFireAlive())
        {
            AudioManager.Current.PlayFireBurning();
        }
        else
        {
            AudioManager.Current.StopFireBurning();
        }
    }

    public bool IsAFireAlive()
    {
        bool firesAlive = false;
        foreach (GameObject fire in Fires)
        {
            if (fire.GetComponent<FireController>().FireStatus == FireStatus.burning)
            {
                firesAlive = true;
            }
        }
        return firesAlive;
    }
    
}