using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TearsController : MonoBehaviour
{
    public ParticleSystem TearParticles;
    public List<ParticleCollisionEvent> collisionEvents;

    void Start()
    {
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void Update()
    {
    }

    void OnParticleCollision(GameObject other)
    {
        AudioManager.Current.PlayWaterDrop();
        
        BucketController bucket = other.gameObject.GetComponentInChildren<BucketController>();
        if (bucket != null)
        {
            bucket.CollectRain();
        }
        
        FireController controller = other.GetComponent<FireController>();
        if (controller != null)
        {
            controller.WaterCollision();
        }
    }
}
