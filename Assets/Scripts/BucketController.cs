using UnityEngine;

public class BucketController : MonoBehaviour
{
    public int RainCount;
    public float RainCollectDelay;
    public int BucketSize;
    public SpriteRenderer PlayerRenderer;
    private float lastCollectTime = 0f;
    public Sprite EmptyBucket;
    public Sprite FullBucket;

    public PlayerRotation PlayerRotation;
    public ParticleSystem PourParticles;

    private void Update()
    {
        Vector3 rotation = PlayerRotation.GetCurrentRotation();
        if (rotation.z < 280 && rotation.z > 90 && RainCount > 0)
        {
            PourBucket();
        }
    }

    public void PourBucket()
    {
        RainCount = 0;
        PourParticles.Play();
        CheckIfFull();
    }

    public void CollectRain()
    {
        Debug.Log("Rain Collected");
        if (lastCollectTime + RainCollectDelay < Time.time && RainCount < BucketSize)
        {
            RainCount++;
            lastCollectTime = Time.time;
        }
        CheckIfFull();
    }

    private void CheckIfFull()
    {
        if (RainCount >= BucketSize)
        {
            RainCount = BucketSize;
            PlayerRenderer.sprite = FullBucket;
        }
        else
        {
            PlayerRenderer.sprite = EmptyBucket;
        }
    }
}
