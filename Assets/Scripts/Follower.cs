using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform Target;

    private void FixedUpdate()
    {
        this.transform.position = Target.position;
        this.transform.rotation = Target.rotation;
    }
}
