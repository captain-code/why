using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput : MonoBehaviour
{
    public static PlayerInput current;

    public bool InputEnabled = true;

    public PlayerInputActions InputMaster;
    private InputAction RotateLeftAction;
    private InputAction RotateRightAction;

    public Vector2 MovementVector
    {
        get
        {
            if (InputEnabled)
            {
                return InputMaster.PlayerActionMap.Movement.ReadValue<Vector2>();
            }

            else
            {
                return Vector2.zero;
            }
        }
    }

    public bool RotatingClockwise = false;
    public bool RotatingAnticlock = false;

    void Awake()
    {
        current = this;
        SetupInput();
    }

    private void SetupInput()
    {
        InputMaster = new PlayerInputActions();
        InputMaster.Enable();
        InputMaster.PlayerActionMap.Movement.Enable();
        InputMaster.PlayerActionMap.RotateLeft.Enable();
        InputMaster.PlayerActionMap.RotateRight.Enable();

        InputMaster.PlayerActionMap.RotateLeft.performed += RotateLeft;
        InputMaster.PlayerActionMap.RotateRight.performed += RotateRight;


        InputMaster.PlayerActionMap.RotateLeft.canceled += StopRotateLeft;
        InputMaster.PlayerActionMap.RotateRight.canceled += StopRotateRight;
    }


    private void OnDestroy()
    {
        TearDownInput();
    }

    private void TearDownInput()
    {
        InputMaster.PlayerActionMap.RotateLeft.performed -= RotateLeft;
        InputMaster.PlayerActionMap.RotateRight.performed -= RotateRight;
    }

    public void FreezeInput()
    {
        InputEnabled = false;
        RotatingAnticlock = false;
        RotatingClockwise = false;

    }

    private void RotateLeft(InputAction.CallbackContext obj)
    {
        if (InputEnabled)
            RotatingAnticlock = true;
    }

    private void StopRotateLeft(InputAction.CallbackContext obj)
    {
        if (InputEnabled)
            RotatingAnticlock = false;
    }

    private void RotateRight(InputAction.CallbackContext obj)
    {
        if (InputEnabled)
            RotatingClockwise = true;
    }

    private void StopRotateRight(InputAction.CallbackContext obj)
    {
        if (InputEnabled)
            RotatingClockwise = false;
    }
}