using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucketWaterController : MonoBehaviour
{
   
    public List<ParticleCollisionEvent> collisionEvents;

    void Start()
    {
        collisionEvents = new List<ParticleCollisionEvent>();
    }
    
       
    void OnParticleCollision(GameObject other)
    {
        FireController controller = other.GetComponent<FireController>();
        if (controller != null)
        {
            controller.WaterCollision();
        }
    }
}
