using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    public float RotationSpeed = 1f;

    public Vector3 GetCurrentRotation()
    {
        return transform.rotation.eulerAngles;
        //TODO: Raycast instead?
    }

    private void FixedUpdate()
    {
        Rotate();
    }

    private void Rotate()
    {
        Vector3 rotationToApply = Vector3.zero;
        if (PlayerInput.current.RotatingAnticlock)
        {
            rotationToApply += new Vector3(0, 0, RotationSpeed);
        }

        if (PlayerInput.current.RotatingClockwise)
        {
            
            rotationToApply += new Vector3(0, 0, -RotationSpeed);
        }

        if (rotationToApply != Vector3.zero)
        {
            transform.Rotate(rotationToApply, Space.Self);
        }
    }
}
