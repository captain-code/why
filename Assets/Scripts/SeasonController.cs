using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeasonController : StateMachineBehaviour
{
    
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName("lowseason"))  
        {
           animator.SetInteger("Season",(int)SeasonState.lowseason);
        }
        
        if (stateInfo.IsName("Storm"))
        {
            animator.SetInteger("Season",(int)SeasonState.Stormy);
        }
        
        if (stateInfo.IsName("Sun"))
        {
            animator.SetInteger("Season",(int)SeasonState.Day);
        }
    }

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMachineEnter is called when entering a state machine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}

    // OnStateMachineExit is called when exiting a state machine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}
    
    //TODO: Random transition from cloudy to day or storm.
}

public enum SeasonState
{
    Day = 2,
    lowseason = 4,
    Stormy = 6,
}
