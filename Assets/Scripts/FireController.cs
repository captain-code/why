using System;
using UnityEngine;

public class FireController : MonoBehaviour
{
    public Animator FlashAnimator;
    
    public float MaxHealth;
    public float CurrentHealth;
    public float WaterDamage;
    public float LightningRestoreAmount;
    
    public GameObject SpriteObject;

    public float lastRefillTime = 0f;
    public float refillDelay = 1f;
    public float StartHealth;


    private FireStatus fireStatus;
    public FireStatus FireStatus
    {
        get => fireStatus;
        set
        {
            if (fireStatus != value)
            {
                if (value == FireStatus.burning)
                {
                    SpriteObject.SetActive(true);
                    
                }else if (value == FireStatus.dead)
                {
                    SpriteObject.SetActive(false);
                    AudioManager.Current.PlayFireExtinguished();
                }

                fireStatus = value;
                FireDirector.Current.FireStateChanged();
            }
        }
    }

    void Start()
    {
        CurrentHealth = StartHealth;
        FireStatus = FireStatus.dead;
    }

    public void WaterCollision()
    {
        if (fireStatus == FireStatus.burning)
        {
            CurrentHealth -= WaterDamage;
            if (CurrentHealth <= 0)
            {
                FireStatus = FireStatus.dead;
            }
        }
    }

    public void LightningStrike(float health)
    {
        AddHealth(health);
    }

    private void AddHealth(float health)
    {
        CurrentHealth += health;
        FireStatus = FireStatus.burning;
        if (CurrentHealth > MaxHealth)
        {
            CurrentHealth = MaxHealth;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Lightning"))
        {
            if (lastRefillTime + refillDelay < Time.time)
            {
                LightningStrike(LightningRestoreAmount); //TODO: one class
                lastRefillTime = Time.time;
                AudioManager.Current.PlayLightningClip();
                FlashAnimator.Play("LightningFlash");
            }
        }
    }
}

public enum FireStatus
{
    dead = 0,
    burning = 1,
}