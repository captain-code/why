using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Current;
    
    public AudioSource EffectsSource;
    public AudioSource MusicSource;
    public AudioSource FireSource;

    public AudioClip MusicClip;
    public AudioClip[] LightningClips;
    public AudioClip[] WaterDropClips;
    public AudioClip FireExtinguishedClip;
    public AudioClip FireBurningClip;
    
    void Awake()
    {
        Current = this;
    }

    public void PlayLightningClip()
    {
        int x = Random.Range(0, LightningClips.Length -1);
        EffectsSource.PlayOneShot(LightningClips[x]);
    }

    public void PlayWaterDrop()
    {
        int x = Random.Range(0, WaterDropClips.Length -1);
        EffectsSource.PlayOneShot(WaterDropClips[x]);
    }
    
    public void PlayFireExtinguished()
    {
        EffectsSource.PlayOneShot(FireExtinguishedClip);
    }

    public void PlayFireBurning()
    {
        if (!FireSource.isPlaying)
        {
            FireSource.clip = FireBurningClip;
            FireSource.loop = true;
            FireSource.Play();
        }
    }

    public void StopFireBurning()
    {
        FireSource.Stop();
    }
    
}
