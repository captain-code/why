using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Animator FlashAnimator;
    
    public AudioSource audio;
    public BucketController PlayerBucket;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Lightning"))
        {
            AudioManager.Current.PlayLightningClip();
            StartCoroutine("PlayerDeath");
        }
    }
    
    private IEnumerator PlayerDeath()
    {
        yield return new WaitForSeconds(0.2f);
        FlashAnimator.Play("DeathFlash");
        PlayerBucket.PourBucket();
        audio.mute = true;
        PlayerInput.current.FreezeInput();
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}